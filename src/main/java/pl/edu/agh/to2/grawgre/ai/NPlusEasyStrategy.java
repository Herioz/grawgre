package pl.edu.agh.to2.grawgre.ai;

import java.util.List;

/**
 * Created by jakubtustanowski on 10/01/2017.
 */
public class NPlusEasyStrategy extends NStrategy {
    public NPlusEasyStrategy(String nick) {
        super(nick);
    }


    @Override
    protected double assertHandValue(List<Integer> tempDice, int toWin, int numberOfRerolls) {
        int sum = 0;
        for(Integer die : tempDice){
            sum += die;
        }
        if(sum==toWin) return 1.0;
        return 0;
    }
}
