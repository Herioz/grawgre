package pl.edu.agh.to2.grawgre.server.configuration;

import pl.edu.agh.to2.grawgre.server.controller.MessageController;
import pl.edu.agh.to2.grawgre.server.controller.Server;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * Created by luke on 10.01.17.
 */

@SpringBootApplication
public class Main {

    public static void main(String[] args) {

        ConfigurableApplicationContext context = SpringApplication.run(MainConfiguration.class, args);

        Server server = context.getBean(Server.class);
        MessageController messageController = context.getBean(MessageController.class);

//        server.addClient(new LoginContainer("test", "test123_queue"));
//        System.out.println(server.checkNick("test"));
//        System.out.println(server.checkNick("cos"));
//
//
//        JmsTemplate jmsTemplate = context.getBean(JmsTemplate.class);
//        jmsTemplate.convertAndSend(QueueNames.LOGIN_QUEUE, new LoginContainer("kappa", "lul"));

    }

}
