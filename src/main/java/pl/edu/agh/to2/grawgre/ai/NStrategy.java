package pl.edu.agh.to2.grawgre.ai;

import pl.edu.agh.to2.grawgre.model.GameState;
import pl.edu.agh.to2.grawgre.model.Move;
import pl.edu.agh.to2.grawgre.model.Player;

import java.util.*;

/**
 * Created by Ola on 2017-01-11.
 */
public abstract class NStrategy extends Strategy {
    private Map<Integer, Map<Integer, Integer>> diceProbabilities;
    public NStrategy(String nick) {
        super(nick);
        diceProbabilities = new HashMap<>();
    }
    public Move makeMove(GameState gameState){
        Player self = getPlayer(gameState.getListOfPlayers());
        List<Integer> selfDice = self.getDice();
        if(isListEmpty(selfDice))
            return fullReroll();
        Double[] goodRerolls = new Double[32];
        Double[] allRerolls  = new Double[32];
        Arrays.fill(goodRerolls, 0.0);
        Arrays.fill(allRerolls, 0.0);
        checkAndGoDeeper(0,selfDice, goodRerolls, allRerolls, 0, gameState.getNumberOfPointsToWin());
        int finalHash = findMaxHash(goodRerolls,allRerolls);
        return hashToMove(finalHash);
    }

    protected  void checkAndGoDeeper(int currentDice, List<Integer> tempDice, Double[] goodRerolls, Double[] allRerolls, int rerollHash, int toWin){
        int myRerollHash;

        for(int i=0;i<6;i++)
        {
            tempDice.set(currentDice, nextDieValue(tempDice.get(currentDice)));
            if(i==5)
            {
                myRerollHash = setRerollHash(rerollHash, currentDice, false);
            }
            else
            {
                myRerollHash = setRerollHash(rerollHash, currentDice, true);
            }
            //System.out.println(myRerollHash);
            allRerolls[myRerollHash]++;

            if (assertHandValue(tempDice, toWin, getNumberOfRerolls(rerollHash))>0) {
                goodRerolls[myRerollHash]+=assertHandValue(tempDice, toWin, getNumberOfRerolls(rerollHash));
            }
            if(currentDice<4)
                checkAndGoDeeper(currentDice+1, tempDice, goodRerolls, allRerolls, myRerollHash, toWin);
        }
    }

    protected  int getNumberOfRerolls(int rerollHash){
        Set<Integer> reroll = hashToMove(rerollHash).getDice();
        return reroll.size();
    }

    protected abstract double assertHandValue(List<Integer> tempDice, int toWin, int numberOfRerolls);

}
