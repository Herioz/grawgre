package pl.edu.agh.to2.grawgre.server.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import pl.edu.agh.to2.grawgre.container.*;
import pl.edu.agh.to2.grawgre.model.Game;
import pl.edu.agh.to2.grawgre.model.GameState;
import pl.edu.agh.to2.grawgre.server.model.ServerHuman;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by luke on 21.01.17.
 */

@RestController
public class RestMessageController {

    @Autowired
    private Server server;

    @Autowired
    private RestTemplateBuilder restTemplateBuilder;

    private Map<Boolean, HttpStatus> statusMap;

    public RestMessageController(){
        statusMap = new TreeMap<>();
        statusMap.put(true, HttpStatus.OK);
        statusMap.put(false, HttpStatus.CONFLICT);
    }

    @PostMapping("/" + QueueNames.LOGIN_QUEUE)
    public ResponseEntity<?> login(@RequestBody RestLoginContainer container){
        Boolean isAvailable = server.checkNick(container.getNick());
        if(!isAvailable){
            return ResponseEntity.status(statusMap.get(false)).build();
        }else{
            server.addRestClient(container);
            return ResponseEntity.status(statusMap.get(true)).build();
        }
    }

    @GetMapping("/" + QueueNames.GETGAMES_QUEUE)
    public List<Game> getGames(){
        return server.getAllGames();
    }

    @PostMapping("/" + QueueNames.CREATEGAME_QUEUE)
    public ResponseEntity<?> createGame(@RequestBody CreateGameContainer container){
        Boolean result = server.handleCreateGame(container);
        return ResponseEntity.status(statusMap.get(result)).build();
    }

    @PostMapping("/" + QueueNames.JOINASPLAYER_QUEUE)
    public ResponseEntity<?> joinAsPlayer(@RequestBody JoinContainer container){
        Boolean result = server.handleJoinAsPlayer(container);
        return ResponseEntity.status(statusMap.get(result)).build();
    }

    @PostMapping("/" + QueueNames.SPECTATEGAME_QUEUE)
    public ResponseEntity<?> spectateGame(JoinContainer container){
        Boolean result = server.handleSpectateGame(container);
        return ResponseEntity.status(statusMap.get(result)).build();
    }

    @PostMapping("/" + QueueNames.MAKEMOVE_QUEUE)
    public ResponseEntity<?> makeMove(MakeMoveContainer container){
        Boolean result = server.handleMakeMove(container);
        return ResponseEntity.status(statusMap.get(result)).build();
    }

    @PostMapping("/" + QueueNames.QUITGAME_QUEUE)
    public ResponseEntity<?> quitGame(@RequestBody String nick){
        server.handleQuitGame(nick);
        return ResponseEntity.status(statusMap.get(true)).build();
    }

    public void updatePerson(ServerHuman serverHuman, GameState gameState){
        restTemplateBuilder.rootUri(serverHuman.getUrl()).build()
                .postForEntity("/" + QueueNames.UPDATEGAME_QUEUE, gameState, ResponseEntity.class);
    }

}
