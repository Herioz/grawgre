package pl.edu.agh.to2.grawgre.server.configuration;

import pl.edu.agh.to2.grawgre.ai.BotFactoryAI;
import pl.edu.agh.to2.grawgre.logic.ConcreteGameFactory;
import pl.edu.agh.to2.grawgre.server.controller.MessageController;
import pl.edu.agh.to2.grawgre.server.controller.Server;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.edu.agh.to2.grawgre.interfaces.BotFactory;
import pl.edu.agh.to2.grawgre.interfaces.GameFactory;

import static org.mockito.Mockito.mock;

/**
 * Created by luke on 10.01.17.
 */

@Configuration
public class ServerConfiguration {

    @Bean
    public Server server(GameFactory gameFactory, BotFactory botFactory){
        Server server = Server.getInstance(gameFactory, botFactory);
        return server;
    }

    @Bean
    public MessageController messageController(){
        MessageController messageController = new MessageController();
        return messageController;
    }

    @Bean
    public GameFactory gameFactory(){
        return new ConcreteGameFactory();
    }

    @Bean
    public BotFactory botFactory(){
        return new BotFactoryAI();
    }

}
