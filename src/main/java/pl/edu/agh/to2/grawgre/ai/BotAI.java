package pl.edu.agh.to2.grawgre.ai;

import pl.edu.agh.to2.grawgre.interfaces.Bot;
import pl.edu.agh.to2.grawgre.model.GameState;
import pl.edu.agh.to2.grawgre.model.Move;

/**
 * Created by jakubtustanowski on 10/01/2017.
 */
public class BotAI extends Bot {

    public BotAI(String nick, Strategy strategy)
    {
        super();
        this.strategy = strategy;
        this.setNick(nick);

    }

    private Strategy strategy;

    @Override
    public Move makeMove(GameState gameState) {
        return strategy.makeMove(gameState);
    }

    public Strategy getStrategy() {
        return strategy;
    }
}
