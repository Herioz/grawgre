package pl.edu.agh.to2.grawgre.server.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * Created by luke on 10.01.17.
 */

@Configuration
@Import({ServerConfiguration.class, MessagingConfiguration.class})
public class MainConfiguration {
}
