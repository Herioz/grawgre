package pl.edu.agh.to2.grawgre.server.model;

import pl.edu.agh.to2.grawgre.server.controller.MessageController;
import pl.edu.agh.to2.grawgre.server.controller.Server;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import pl.edu.agh.to2.grawgre.interfaces.BotFactory;
import pl.edu.agh.to2.grawgre.interfaces.GameFactory;
import pl.edu.agh.to2.grawgre.model.GameState;
import pl.edu.agh.to2.grawgre.model.GameStatus;
import pl.edu.agh.to2.grawgre.model.Move;
import pl.edu.agh.to2.grawgre.model.Player;

import javax.naming.TimeLimitExceededException;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;

/**
 * Created by luke on 10.01.17.
 */
public class ServerHumanTest {
    private Server server;
    private ServerHuman serverHuman;
    private final String creatorNick = "creator";

    @Mock
    private GameFactory gameFactory;

    @Mock
    private BotFactory botFactory;

    @Mock
    private MessageController messageController;

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Before
    public void setUp() {
        server = new Server(gameFactory, botFactory);
        server.setMessageController(messageController);
        serverHuman = new ServerHuman(creatorNick);
    }

    @Test (expected = TimeLimitExceededException.class)
    public void testTimeForMoveExceeded() throws TimeLimitExceededException, InterruptedException {
        BlockingQueue<Object[]> taskQueue = new LinkedBlockingQueue<>();
        BlockingQueue<Boolean> resultQueue = new LinkedBlockingQueue<>();
        GameState gameState = new GameState(new LinkedList<Player>(), GameStatus.STARTED, "whatever");
        Move move = serverHuman.makeMove(gameState, taskQueue, resultQueue);
    }

    @Test
    public void testMakeMoveWithEverythingOk() {
        BlockingQueue<Object[]> taskQueue = new LinkedBlockingQueue<>(1);
        BlockingQueue<Boolean> resultQueue = new LinkedBlockingQueue<>(1);
        GameState gameState = new GameState(new LinkedList<Player>(), GameStatus.STARTED, "whatever");
        Object[] o = new Object[2];
        o[0] = serverHuman;
        o[1] = new Move(new HashSet<Integer>());
        taskQueue.add(o);

        try {
            Move move = serverHuman.makeMove(gameState, taskQueue, resultQueue);
            assertEquals(true, resultQueue.poll(10, TimeUnit.MILLISECONDS));
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (TimeLimitExceededException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testMakeMoveWhenOtherPlayerSentMove() {
        BlockingQueue<Object[]> taskQueue = new LinkedBlockingQueue<>(1);
        BlockingQueue<Boolean> resultQueue = new LinkedBlockingQueue<>(1);
        GameState gameState = new GameState(new LinkedList<Player>(), GameStatus.STARTED, "whatever");
        Object[] o = new Object[2];
        o[0] = new ServerHuman("intruder");
        o[1] = new Move(new HashSet<Integer>());
        taskQueue.add(o);

        try {
            Move move = serverHuman.makeMove(gameState, taskQueue, resultQueue);
            assertEquals(resultQueue.poll(10, TimeUnit.MILLISECONDS), false);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (TimeLimitExceededException e) {
            e.printStackTrace();
        }
    }
}
