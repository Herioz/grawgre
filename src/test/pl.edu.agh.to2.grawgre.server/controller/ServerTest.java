package pl.edu.agh.to2.grawgre.server.controller;

import pl.edu.agh.to2.grawgre.server.exception.ElementNotFoundException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import pl.edu.agh.to2.grawgre.container.CreateGameContainer;
import pl.edu.agh.to2.grawgre.container.JoinContainer;
import pl.edu.agh.to2.grawgre.container.LoginContainer;
import pl.edu.agh.to2.grawgre.container.MakeMoveContainer;
import pl.edu.agh.to2.grawgre.exception.MoveNotAllowedException;
import pl.edu.agh.to2.grawgre.exception.OperationNotAllowedException;
import pl.edu.agh.to2.grawgre.interfaces.Bot;
import pl.edu.agh.to2.grawgre.interfaces.BotFactory;
import pl.edu.agh.to2.grawgre.interfaces.GameController;
import pl.edu.agh.to2.grawgre.interfaces.GameFactory;
import pl.edu.agh.to2.grawgre.model.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * Created by luke on 10.01.17.
 */
public class ServerTest {

    private Server server;

    @Mock
    private GameFactory gameFactory;
    @Mock
    private BotFactory botFactory;

    @Mock
    private MessageController messageController;

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();


    @Before
    public void setUp() {
        server = new Server(gameFactory, botFactory);
        server.setMessageController(messageController);
    }

    @Test
    public void testAddClient() {
        String nick = "test";

        server.addClient(new LoginContainer(nick, ""));

        assertFalse(server.checkNick(nick));
    }

    @Test
    public void testCorrectLogin() {
        String nick = "test";
        String respond = "testRespond";
        LoginContainer lc = new LoginContainer(nick, respond);

        assertTrue(server.checkNick(nick));
        server.addClient(lc);
        assertFalse(server.checkNick(nick));
    }

    @Test
    public void testGetNoneGames() {
        assertEquals(server.getAllGames().size(), 0);
    }

    @Test
    public void testCorrectCreateGame() {
        String nick = "test";
        GameController gameController = mock(GameController.class);
        Game game = mock(Game.class);
        Configuration conf = mock(Configuration.class);
        GameState gameState = new GameState(new ArrayList<Player>(), GameStatus.LOBBY, "");

        when(botFactory.makeBots(any(Configuration.class))).thenReturn(new ArrayList<Bot>(0));
        when(gameFactory.createGame(any(Configuration.class), any(Player.class), any(List.class))).thenReturn(gameController);
        when(gameController.getGame()).thenReturn(game);
        when(game.getGameID()).thenReturn(1);
        when(game.getGameState()).thenReturn(gameState);

        server.addClient(new LoginContainer(nick, ""));
        assertTrue(server.handleCreateGame(new CreateGameContainer(nick, conf)));
    }

    @Test
    public void testUnknownGameHandleJoinAsPlayer() {
        String nick = "test";

        server.addClient(new LoginContainer(nick, ""));
        assertFalse(server.handleJoinAsPlayer(new JoinContainer(nick, 99)));
    }

    @Test
    public void testPlayerAlreadyInGameHandleJoinAsPlayer() {
        String nick = "test";
        Integer gameID = 1;
        GameController gameController = mock(GameController.class);
        Game game = mock(Game.class);
        Configuration conf = mock(Configuration.class);
        GameState gameState = new GameState(new ArrayList<Player>(), GameStatus.LOBBY, "");

        when(botFactory.makeBots(any(Configuration.class))).thenReturn(new ArrayList<Bot>(0));
        when(gameFactory.createGame(any(Configuration.class), any(Player.class), any(List.class))).thenReturn(gameController);
        when(gameController.getGame()).thenReturn(game);
        when(game.getGameID()).thenReturn(gameID);
        when(game.getGameState()).thenReturn(gameState);

        server.addClient(new LoginContainer(nick, ""));
        assertTrue(server.handleCreateGame(new CreateGameContainer(nick, conf)));
        assertFalse(server.handleJoinAsPlayer(new JoinContainer(nick, gameID)));
    }

    @Test
    public void testNotAllowedHandleJoinAsPlayer() throws OperationNotAllowedException {
        String joinNick = "test2";
        String nick = "test";
        GameController gameController = mock(GameController.class);
        Game game = mock(Game.class);
        Configuration conf = mock(Configuration.class);
        GameState gameState = new GameState(new ArrayList<Player>(), GameStatus.LOBBY, "");

        when(botFactory.makeBots(any(Configuration.class))).thenReturn(new ArrayList<Bot>(0));
        when(gameFactory.createGame(any(Configuration.class), any(Player.class), any(List.class))).thenReturn(gameController);
        when(gameController.getGame()).thenReturn(game);
        when(game.getGameID()).thenReturn(1);
        when(game.getGameState()).thenReturn(gameState);

        when(gameController.addPlayer(any(Player.class))).thenThrow(OperationNotAllowedException.class);

        server.addClient(new LoginContainer(nick, ""));
        server.addClient(new LoginContainer(joinNick, ""));
        assertTrue(server.handleCreateGame(new CreateGameContainer(nick, conf)));
        assertFalse(server.handleJoinAsPlayer(new JoinContainer(joinNick, 1)));
    }

    @Test
    public void testCorrectHandleJoinAsPlayer() throws OperationNotAllowedException, ElementNotFoundException {
        String joinNick = "test2";
        String nick = "test";
        Integer gameID = 1;
        GameController gameController = mock(GameController.class);
        Game game = mock(Game.class);
        Configuration conf = mock(Configuration.class);
        GameState gameState = new GameState(new ArrayList<Player>(), GameStatus.LOBBY, "");

        when(botFactory.makeBots(any(Configuration.class))).thenReturn(new ArrayList<Bot>(0));
        when(gameFactory.createGame(any(Configuration.class), any(Player.class), any(List.class))).thenReturn(gameController);
        when(gameController.getGame()).thenReturn(game);
        when(game.getGameID()).thenReturn(gameID);
        when(game.getGameState()).thenReturn(gameState);

        when(gameController.addPlayer(any(Player.class))).thenReturn(gameState);

        server.addClient(new LoginContainer(nick, ""));
        server.addClient(new LoginContainer(joinNick, ""));
        assertTrue(server.handleCreateGame(new CreateGameContainer(nick, conf)));
        assertTrue(server.handleJoinAsPlayer(new JoinContainer(joinNick, gameID)));
        verify(messageController, times(1)).updateAllPeople(eq(gameState), any(ServerGameController.class));
    }

    @Test
    public void testPlayerAlreadyInGameHandleSpectateGame() {
        String nick = "test";
        GameController gameController = mock(GameController.class);
        Game game = mock(Game.class);
        Configuration conf = mock(Configuration.class);
        GameState gameState = new GameState(new ArrayList<Player>(), GameStatus.LOBBY, "");

        when(botFactory.makeBots(any(Configuration.class))).thenReturn(new ArrayList<Bot>(0));
        when(gameFactory.createGame(any(Configuration.class), any(Player.class), any(List.class))).thenReturn(gameController);
        when(gameController.getGame()).thenReturn(game);
        when(game.getGameID()).thenReturn(1);
        when(game.getGameState()).thenReturn(gameState);

        server.addClient(new LoginContainer(nick, ""));
        assertTrue(server.handleCreateGame(new CreateGameContainer(nick, conf)));
        assertFalse(server.handleSpectateGame(new JoinContainer(nick, 1)));
    }

    @Test
    public void testUnknownGameHandleSpectateGame() {
        String nick = "test";

        server.addClient(new LoginContainer(nick, ""));
        assertFalse(server.handleSpectateGame(new JoinContainer(nick, 1)));
    }

    @Test
    public void testCorrectHandleSpectateGame() {
        String spectateNick = "test2";
        String nick = "test";
        Integer gameID = 1;
        GameController gameController = mock(GameController.class);
        Game game = mock(Game.class);
        Configuration conf = mock(Configuration.class);
        GameState gameState = new GameState(new ArrayList<Player>(), GameStatus.LOBBY, "");

        when(botFactory.makeBots(any(Configuration.class))).thenReturn(new ArrayList<Bot>(0));
        when(gameFactory.createGame(any(Configuration.class), any(Player.class), any(List.class))).thenReturn(gameController);
        when(gameController.getGame()).thenReturn(game);
        when(game.getGameID()).thenReturn(gameID);
        when(game.getGameState()).thenReturn(gameState);

        server.addClient(new LoginContainer(nick, ""));
        server.addClient(new LoginContainer(spectateNick, ""));
        assertTrue(server.handleCreateGame(new CreateGameContainer(nick, conf)));
        assertTrue(server.handleSpectateGame(new JoinContainer(spectateNick, gameID)));
    }

    @Test
    public void testCorrectJoinAsPlayer() {
        String creatorNick = "creator", joinNick = "test";
        GameController gameController = mock(GameController.class);
        Game game = mock(Game.class);
        Configuration conf = mock(Configuration.class);
        GameState gameState = new GameState(new ArrayList<Player>(), GameStatus.LOBBY, "");

        when(botFactory.makeBots(any(Configuration.class))).thenReturn(new ArrayList<Bot>(0));
        when(gameFactory.createGame(any(Configuration.class), any(Player.class), any(List.class))).thenReturn(gameController);
        when(gameController.getGame()).thenReturn(game);
        when(game.getGameID()).thenReturn(1);
        when(game.getGameState()).thenReturn(gameState);
        try {
            when(gameController.addPlayer(any(Player.class))).thenReturn(gameState);
        } catch (OperationNotAllowedException e) {
            System.out.println("NOOOOOO");
        }

        server.addClient(new LoginContainer(creatorNick, ""));
        server.addClient(new LoginContainer(joinNick, ""));
        server.handleCreateGame(new CreateGameContainer(creatorNick, conf));
        assertTrue(server.handleJoinAsPlayer(new JoinContainer(joinNick, 1)));
    }

    @Test
    public void testCorrectSpectate() {
        String creatorNick = "creator", spectatorNick = "test";
        GameController gameController = mock(GameController.class);
        Game game = mock(Game.class);
        Configuration conf = mock(Configuration.class);
        GameState gameState = new GameState(new ArrayList<Player>(), GameStatus.LOBBY, "");

        when(botFactory.makeBots(any(Configuration.class))).thenReturn(new ArrayList<Bot>(0));
        when(gameFactory.createGame(any(Configuration.class), any(Player.class), any(List.class))).thenReturn(gameController);
        when(gameController.getGame()).thenReturn(game);
        when(game.getGameID()).thenReturn(1);
        when(game.getGameState()).thenReturn(gameState);
        try {
            when(gameController.addPlayer(any(Player.class))).thenReturn(gameState);
        } catch (OperationNotAllowedException e) {
            System.out.println("NOOOOOO");
        }

        server.addClient(new LoginContainer(creatorNick, ""));
        server.addClient(new LoginContainer(spectatorNick, ""));
        server.handleCreateGame(new CreateGameContainer(creatorNick, conf));
        assertTrue(server.handleSpectateGame(new JoinContainer(spectatorNick, 1)));
    }

}